﻿
using Microsoft.SqlServer.Dac;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackupAndRestoreDatabase
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //Install NuGet package Microsoft.SqlServer.DacFx.x64

        //Sample code to export .bacpac file
        private void ExportBacpac()
        {
            DacServices ds = new DacServices("connectionString"); // database connection string

            ds.ExportBacpac(@"C:\Data\BackupTest123.bacpac", "NewDatabaseName"); // location and file name
        }

        //Sample code to import .bacpac file
        private void ImportBacpac()
        {
            DacServices ds = new DacServices("connectionString"); // database connection string

            var backPackage = BacPackage.Load(@"C:\Data\BackupTest123.bacpac"); // location and file name

            ds.ImportBacpac(backPackage, "NewDatabaseName");
        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            lbPercent.Text = "";

            Server dbServer = new Server(
                new ServerConnection(txtServer.Text, txtUsername.Text, txtPassword.Text));

            Backup dbBackup = new Backup() { Action = BackupActionType.Database, Database = txtDatabase.Text };

            dbBackup.Devices.AddDevice(@"C:\Data\BackupTest.bak", DeviceType.File);

            dbBackup.Initialize = true;

            dbBackup.PercentComplete += DbBackup_PercentComplete;

            dbBackup.SqlBackupAsync(dbServer);
        }



        private void DbBackup_PercentComplete(object sender, PercentCompleteEventArgs e)
        {
            lbPercent.Invoke((MethodInvoker)delegate
            {
                lbPercent.Text = $"{e.Percent}% Complete";
            });
        }


        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Database restore";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                txtFilenameRestore.Text = dlg.FileName;
            }
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            try
            {
                lbPercent.Text = "";

                Server dbServer = new Server(
                    new ServerConnection(txtServer.Text, txtUsername.Text, txtPassword.Text));

                Restore dbRestore = new Restore()
                {
                    Database = txtDatabase.Text,
                    Action = RestoreActionType.Database,
                    ReplaceDatabase = true,
                    NoRecovery = false
                };

                dbServer.KillAllProcesses(dbRestore.Database);

                dbRestore.Devices.AddDevice(txtFilenameRestore.Text, DeviceType.File);

                dbRestore.PercentComplete += DbBackup_PercentComplete;

                dbRestore.SqlRestoreAsync(dbServer);
            }
            catch(Exception ex)
            {

            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
